
//- Project : HP34401
//- file : HPManager.cpp

#include "task/HPManager.hpp"
#include <iomanip>
#include <yat/utils/XString.h>  //- to_num
#include <yat/utils/String.h>   //- to_upper
#include <yat/Portability.h>    //- NaN
#include <yat/time/Timer.h>     //- perf

// ========================================================
const std::size_t PERIODIC_MSG_PERIOD    = 5000;            //- in ms
const std::size_t MESSAGE_TIMEOUT        = 2500;
//---------------------------------------------------------
//- the YAT user messages
const std::size_t EXEC_LOW_LEVEL_MSG     = yat::FIRST_USER_MSG + 1000;
const std::size_t SET_TRIGGER_COUNT_MSG  = yat::FIRST_USER_MSG + 1010;
const std::size_t GET_TRIGGER_COUNT_MSG  = yat::FIRST_USER_MSG + 1011;
const std::size_t SET_SAMPLES_COUNT_MSG  = yat::FIRST_USER_MSG + 1012;
const std::size_t GET_SAMPLES_COUNT_MSG  = yat::FIRST_USER_MSG + 1013;
const std::size_t SET_NPLC_MSG           = yat::FIRST_USER_MSG + 1014;
const std::size_t GET_NPLC_MSG           = yat::FIRST_USER_MSG + 1015;
const std::size_t SET_TRIGGER_SOURCE_MSG = yat::FIRST_USER_MSG + 1016;
const std::size_t GET_TRIGGER_SOURCE_MSG = yat::FIRST_USER_MSG + 1017;
const std::size_t GET_RESOLUTION_MSG     = yat::FIRST_USER_MSG + 1020;
const std::size_t GET_RANGE_MSG          = yat::FIRST_USER_MSG + 1021;
const std::size_t SET_AUTO_RANGE_MSG     = yat::FIRST_USER_MSG + 1030;
const std::size_t GET_AUTO_RANGE_MSG     = yat::FIRST_USER_MSG + 1031;
//---------------------------------------------------------
//- configuration
const std::string CMD_VOLTAGE_DC_MODE    = "FUNC \"VOLT:DC\"";
const std::string MODE_PREFIX            = "VOLT:DC";
const Tango::DevUShort MIN_TRIGGER_COUNT = 1;
const Tango::DevUShort MAX_TRIGGER_COUNT = 50000;
const Tango::DevUShort MIN_SAMPLE_COUNT  = 1;
const Tango::DevUShort MAX_SAMPLE_COUNT  = 50000;
const Tango::DevDouble DEFAULT_NPLC      = 1.;
const std::string DEFAULT_TRIGGER_SRC    = "IMM";           //- IMMediate
const std::string COMMENT                = "#";
//---------------------------------------------------------
//- commands
const std::string CMD_ENABLE_AUTORANGE   = ":RANG:AUTO ON";
const std::string CMD_DISABLE_AUTORANGE  = ":RANG:AUTO OFF";
const std::string CMD_IS_AUTORANGE_ON    = ":RANG:AUTO?";
const std::string CMD_READ_ERROR         = "SYST:ERR?";
const std::string CMD_START_INTEGRATION  = "INIT";
const std::string CMD_STOP_INTEGRATION   = "DCL";           //- device clear message!
const std::string CMD_READ_LAST_VALUE    = "INIT;FETCH?";
const std::string NO_ERROR               = "No error";
//---------------------------------------------------------
const long SLEEP_IN_SECONDS              = 0;
const long SLEEP_BETWEEN_TWO_ACTIONS     = 50000000;        //- 50 milli seconds
//---------------------------------------------------------

namespace HP34401A_ns
{
//-----------------------------------------------
//- Ctor ----------------------------------------
HPManager::HPManager (Tango::DeviceImpl * host_device, 
                      std::string proxy_name,
                      std::vector<std::string> config)
: yat4tango::DeviceTask(host_device),
  host_dev (host_device),
  m_proxy_name(proxy_name),
  m_vec_config(config),
  m_com (0),
  m_trigger_count (MIN_TRIGGER_COUNT),
  m_sample_count(MIN_SAMPLE_COUNT),
  m_nplc(DEFAULT_NPLC),
  m_trigger_src(DEFAULT_TRIGGER_SRC),
  m_resolution(0.),
  m_range(0.),
  m_autorange_enabled(false),
  m_output_value(yat::IEEE_NAN),
  m_error(NO_ERROR)
{
  DEBUG_STREAM << "HPManager::HPManager <- " << std::endl;
}


//-----------------------------------------------
//- Dtor ----------------------------------------
HPManager::~HPManager (void)
{
  DEBUG_STREAM << "HPManager::~HPManager <- " << std::endl;
  //- Noop
}

//-----------------------------------------------
//- the user core of the Task -------------------
void HPManager::process_message (yat::Message& _msg)
{
  DEBUG_STREAM << "HPManager::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT =======================
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "HPManager::handle_message::THREAD_INIT::thread is starting up" << std::endl;
      //- "initialization" code goes here
      try
      {
        m_com.reset(new Communication(host_dev, m_proxy_name));

        //- halt a measurement in progress and place the multimeter in the “idle state.”
        // stop_integration();

        //- send default configuration
        // configure();

        //- empty error buffer(if any)
        std::string resp("");
        do
        {
          resp = write_read(CMD_READ_ERROR);
        }
        while(resp.find(NO_ERROR) == std::string::npos);

        //- yat::Task configure optional msg handling
        enable_timeout_msg(false);
        enable_periodic_msg(true);
        set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      }
      catch (...)
      {
        ERROR_STREAM << "HPManager::init_device()*** yat::SocketException caught ***" << std::endl;
        m_com.reset();
      } 
    } 
    break;
    //- TASK_EXIT =======================
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "HPManager::handle_message handling TASK_EXIT thread is quitting" << std::endl;
      
      // stop_integration();

      //- "release" code goes here
      m_com.reset();
    }
    break;
    //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "HPManager::handle_message handling TASK_PERIODIC msg " << std::endl;
      //- code relative to the task's periodic job goes here
      try
      {
        read_value();

        get_error();
      }
      catch(...)
      {
        //- CAREFULL:
        //- if NPLC is set to 100, a (CORBA) timeout exception is sent but the HP34401
        //- put the integrated value in its output buffer. Prior to any other command
        //- it is mandatory to read back the output buffer!!!
        //- A sleep of 2 seconds is the minimum to get the integrated value.
        //- This is the reason the exception is not managed here and why a Read command is sent.
        INFO_STREAM << "\n\n\tPERIODIC caugth [....] ; trying READ ..." << std::endl;
        try
        {
          yat::ThreadingUtilities::sleep(2,0);
          //- direct access to HW
          std::string resp = m_com->read();
          {//- critical section
            yat::AutoMutex<> guard(m_data_mutex);

            m_output_value = yat::XString<Tango::DevDouble>::to_num (resp);
          }
          INFO_STREAM << "\t... received [" << resp << "]\n" << std::endl;
        }
        catch(...)
        {
          ERROR_STREAM << "\t ERROR -> PERIODIC caugth [....] ; sending READ ..." << std::endl;
          m_output_value = yat::IEEE_NAN;
        }
      }

    }
    break;
    //- TASK_TIMEOUT ===================
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here
      ERROR_STREAM << "HPManager::handle_message handling TASK_TIMEOUT msg" << std::endl;
    }
    break;
  //- USER_DEFINED_MSG ================
  //- low level commands (ASCII commands directly passed to the Lakeshore 336 (appending LF at the end)
  case EXEC_LOW_LEVEL_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling EXEC_LOW_LEVEL_MSG msg" << std::endl;
      // LowLevelMsg * llm = 0;
      // _msg.detach_data(llm);
      // if (llm)
      // {
      //   this->exec_low_level_command (llm->cmd);
      //   delete llm;
      // }
    }
    break;
  case SET_TRIGGER_COUNT_MSG:
      {
        DEBUG_STREAM << "HPManager::handle_message handling SET_TRIGGER_COUNT_MSG msg" << std::endl;
        Tango::DevUShort* value = 0;
        _msg.detach_data(value);
        //- prepare command
        std::stringstream s;
        s << "TRIG:COUN " 
          << *value 
          << std::ends;
        //- send
        write_read (s.str ());
      }
      break;
  case GET_TRIGGER_COUNT_MSG:
      {
        DEBUG_STREAM << "HPManager::handle_message handling GET_TRIGGER_COUNT_MSG msg" << std::endl;

        static std::string cmd("TRIG:COUN?");
        std::string response("");

        response = write_read (cmd);
        
        {//- critical section
          yat::AutoMutex<> guard(m_data_mutex);

          m_trigger_count = yat::XString<Tango::DevUShort>::to_num (response);
        }
      }
      break;
   case SET_SAMPLES_COUNT_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling SET_SAMPLES_COUNT_MSG msg" << std::endl;
      Tango::DevUShort* value = 0;
      _msg.detach_data(value);

      std::stringstream s;
      s << "SAMP:COUN " 
        << *value 
        << std::ends;

      write_read (s.str ());
    }
    break;
   case GET_SAMPLES_COUNT_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling GET_SAMPLES_COUNT_MSG msg" << std::endl;
      static std::string cmd("SAMP:COUN?");
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_sample_count = yat::XString<Tango::DevUShort>::to_num (response);
      }
    }
    break;
  case SET_NPLC_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling SET_NPLC_MSG msg" << std::endl;
      double* value = 0;
      _msg.detach_data(value);

      std::stringstream s;
      s << MODE_PREFIX
        << ":NPLC "
        << *value
        << std::ends;

      write_read (s.str ());
    }
    break;
  case GET_NPLC_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling GET_NPLC_MSG msg" << std::endl;
      static std::string cmd(MODE_PREFIX+":NPLC?");
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_nplc = yat::XString<double>::to_num (response);
      }
    }
    break;
  case SET_TRIGGER_SOURCE_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling SET_TRIGGER_SOURCE_MSG msg" << std::endl;
      std::string& value = _msg.get_data<std::string>();
      // _msg.detach_data(value);
      static std::string cmd("TRIG:SOUR ");

      value = cmd + value;

      write_read (value);
    }
    break;
  case GET_TRIGGER_SOURCE_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling GET_TRIGGER_SOURCE_MSG msg" << std::endl;
      static std::string cmd("TRIG:SOUR?");
      std::string response("");
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_trigger_src = write_read (cmd);
      }
    }
    break;
  case GET_RESOLUTION_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling GET_RESOLUTION_MSG msg" << std::endl;
      static std::string cmd(MODE_PREFIX+":RES?");
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_resolution = yat::XString<double>::to_num (response);
      }
    }
    break;
  case GET_RANGE_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling GET_RANGE_MSG msg" << std::endl;
      static std::string cmd(MODE_PREFIX+":RANG?");
      std::string response("");
      
      response = write_read (cmd);

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_range = yat::XString<double>::to_num (response);
      }
    }
    break;
  case SET_AUTO_RANGE_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling AUTO_RANGE_MSG msg" << std::endl;
      std::string& value = _msg.get_data<std::string>();

      std::stringstream s;
      s << MODE_PREFIX
        << value 
        << std::ends;

      write_read (s.str ());
    }
    break;
  case GET_AUTO_RANGE_MSG:
    {
      DEBUG_STREAM << "HPManager::handle_message handling AUTO_RANGE_MSG msg" << std::endl;
      static std::string cmd(MODE_PREFIX + CMD_IS_AUTORANGE_ON);
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_autorange_enabled = yat::XString<bool>::to_num (response);
      }
    }
    break;
  default:
		  ERROR_STREAM<< "HPManager::handle_message::unhandled msg type received" << std::endl;
		break;
  } //- switch (_msg.type())
} //- HPManager::process_message

//-----------------------------------------------
//- configure HP34401 for acquisition
//-----------------------------------------------
void HPManager::configure()
{
  std::size_t nb_cmd = m_vec_config.size();
  for (std::size_t idx = 0; idx < nb_cmd; idx++)
  {
    //- commands beginning with # are comments
    if (m_vec_config.at(idx).find(COMMENT) != std::string::npos)
    {
      //- skip it
      continue;
    }
    write_read(m_vec_config.at(idx));
  }
}

//-----------------------------------------------
//- set_trigger_count
//-----------------------------------------------
void HPManager::set_trigger_count(Tango::DevUShort trg_cnt)
{
  DEBUG_STREAM << "HPManager::set_trigger_count" << std::endl;
  if (trg_cnt < MIN_TRIGGER_COUNT || trg_cnt > MAX_TRIGGER_COUNT)
  {
    std::stringstream err;
    err << "HPManager::set_trigger_count OUT_OF_RANGE : range not in ["
        << MIN_TRIGGER_COUNT
        << "..."
        << MAX_TRIGGER_COUNT
        << "]."
        << std::ends;

    ERROR_STREAM << err.str() << std::endl;
    Tango::Except::throw_exception ("OUT_OF_RANGE",
                                    err.str(),
                                    "HPManager::set_trigger_count");
  }

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_TRIGGER_COUNT_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::set_trigger_count error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::set_trigger_count");
  }

  msg->attach_data (trg_cnt);
  
  post(msg);
}

//-----------------------------------------------
//- get_trigger_count
//-----------------------------------------------
Tango::DevUShort HPManager::get_trigger_count()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_TRIGGER_COUNT_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_trigger_count error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_trigger_count");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_trigger_count;
}

//-----------------------------------------------
//- set_sample_count : nb samples per trig
//-----------------------------------------------
void HPManager::set_sample_count(Tango::DevUShort samp_cnt)
{
  DEBUG_STREAM << "HPManager::set_sample_count" << std::endl;
  if (samp_cnt < MIN_SAMPLE_COUNT || samp_cnt > MAX_SAMPLE_COUNT)
  {
    std::stringstream err;
    err << "HPManager::set_sample_count OUT_OF_RANGE : range not in ["
        << MIN_SAMPLE_COUNT
        << "..."
        << MAX_SAMPLE_COUNT
        << "]."
        << std::ends;

    ERROR_STREAM << err.str() << std::endl;
    Tango::Except::throw_exception ("OUT_OF_RANGE",
                                    err.str(),
                                    "HPManager::set_sample_count");
  }

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_SAMPLES_COUNT_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::set_sample_count error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::set_sample_count");
  }

  msg->attach_data (samp_cnt);
  
  post(msg);
}

//-----------------------------------------------
//- get_sample_count
//-----------------------------------------------
Tango::DevUShort HPManager::get_sample_count()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_SAMPLES_COUNT_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_sample_count error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_sample_count");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_sample_count;
}

//-----------------------------------------------
//- set_nplc : integration time in NPLC!
//-----------------------------------------------
void HPManager::set_nplc(double nplc)
{
  DEBUG_STREAM << "HPManager::set_nplc" << std::endl;
  if (nplc != 0.02 &&
      nplc != 0.2  && 
      nplc != 1.   && 
      nplc != 10.)
  {
    std::stringstream err;
    err << "HPManager::set_nplc OUT_OF_RANGE : allowed values are ["
        << "0.002|0.2|1|1|10"
        << "].\n NOTE : 100 is too high and slow down the Gpib bus!"
        << std::ends;

    ERROR_STREAM << err.str() << std::endl;
    Tango::Except::throw_exception ("OUT_OF_RANGE",
                                    err.str(),
                                    "HPManager::set_nplc");
  }

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_NPLC_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::set_nplc error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::set_nplc");
  }

  msg->attach_data (nplc);
  
  post(msg);
}

//-----------------------------------------------
//- get_nplc
//-----------------------------------------------
double HPManager::get_nplc()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_NPLC_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_nplc error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_nplc");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_nplc;
}

//-----------------------------------------------
//- set_trigger_source : "BUS", "IMMimeediate" or "EXTernal"
//-----------------------------------------------
void HPManager::set_trigger_source(std::string trg_src)
{
  DEBUG_STREAM << "HPManager::set_trigger_source" << std::endl;
  //- transform in upper case before comparaison
  yat::StringUtil::to_upper(&trg_src);
  //- check
  if (trg_src.find("BUS") == std::string::npos &&
      trg_src.find("IMM") == std::string::npos &&
      trg_src.find("EXT") == std::string::npos)
  {
    std::stringstream err;
    err << "HPManager::set_trigger_source OUT_OF_RANGE : allowed values are ["
        << "\"BUS\", \"IMMediate\" or \"EXTernal\""
        << "]."
        << std::ends;

    ERROR_STREAM << err.str() << std::endl;
    Tango::Except::throw_exception ("OUT_OF_RANGE",
                                    err.str(),
                                    "HPManager::set_trigger_source");
  }

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_TRIGGER_SOURCE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::set_trigger_source error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::set_trigger_source");
  }

  msg->attach_data (trg_src);
  
  post(msg);
}

//-----------------------------------------------
//- get_trigger_source
//-----------------------------------------------
std::string HPManager::get_trigger_source ()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_TRIGGER_SOURCE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_trigger_source error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_trigger_source");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_trigger_src;
}

//-----------------------------------------------
//- get_resolution : number of digits
//-----------------------------------------------
double HPManager::get_resolution ()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_RESOLUTION_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_resolution error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_resolution");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_resolution;
}

//-----------------------------------------------
//- get_range : number of digits
//-----------------------------------------------
double HPManager::get_range ()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_RANGE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::get_range error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::get_range");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_range;
}

//-----------------------------------------------
//- enable_autorange
//-----------------------------------------------
void HPManager::enable_autorange()
{
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(SET_AUTO_RANGE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::enable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::enable_autorange");
  }

  msg->attach_data (CMD_ENABLE_AUTORANGE);
  //-
  post(msg);
}

//-----------------------------------------------
//- disable_autorange
//-----------------------------------------------
void HPManager::disable_autorange()
{
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(SET_AUTO_RANGE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::disable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::disable_autorange");
  }

  msg->attach_data (CMD_DISABLE_AUTORANGE);
  //-
  post(msg);
}

//-----------------------------------------------
//- is_autorange_enabled
//-----------------------------------------------
bool HPManager::is_autorange_enabled()
{
  bool wait = true;
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(GET_AUTO_RANGE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "HPManager::disable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "HPManager::is_autorange_enabled");
  }

  msg->attach_data (CMD_IS_AUTORANGE_ON);
  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_autorange_enabled;
}

//-----------------------------------------------
//- returns state/status
//-----------------------------------------------
Tango::DevState HPManager::get_state_status (std::string& status_)
{
  DEBUG_STREAM << "HPManager::get_state_status <-" << std::endl;
  Tango::DevState argout = Tango::UNKNOWN;
  //- error code separator
  std::string separator(",");
  std::string status("");
 //- case of  device proxy error
  if ( !m_com )
  {
    argout  = Tango::FAULT;
    status_ = "Failed to create proxy!";
  }
  else
  {
    {//- critical section
      yat::AutoMutex<> guard(m_data_mutex);

      status = m_error;
    }
    if (status.find(NO_ERROR) == std::string::npos)
    {
      argout = Tango::ALARM;
    }
    else
    {
      //- erase error code
      std::size_t it = status.find(separator);
      if (it != std::string::npos)
      {
        status.erase(0,it+1);
      }
      argout = Tango::ON;
    }
    status_ = status;
  }

  return argout;
 }

//-----------------------------------------------
//- start_integration
//-----------------------------------------------
void HPManager::start_integration ()
{
  static std::string cmd(CMD_START_INTEGRATION);

  write_read(cmd);
} 
 
//-----------------------------------------------
//- stop_integration
//- NOTE : needs device_clear command from GpibServer
//- which is not implemented yet!!
//-----------------------------------------------
// void HPManager::stop_integration ()
// {
//   static std::string cmd(CMD_STOP_INTEGRATION);

//   write_read(cmd);
// } 
 
//-----------------------------------------------
//- read_value
//-----------------------------------------------
void HPManager::read_value ()
{
  std::string response("");
  static std::string cmd(CMD_READ_LAST_VALUE);

  response = write_read(cmd);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_output_value = yat::XString<Tango::DevDouble>::to_num (response);
  }
} 

//-----------------------------------------------
//- get_error
//-----------------------------------------------
void HPManager::get_error ()
{
  std::string response("");

  response = write_read(CMD_READ_ERROR);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_error = response;
  }
}

//-----------------------------------------------
//- write_read
//- send command returns result
//-----------------------------------------------
std::string HPManager::write_read (std::string cmd)
{
  INFO_STREAM << "HPManager::write_read <- for cmd [" << cmd << "]" << std::endl;
  std::string empty("");
  std::string response("");

  //- case of  device proxy error
  if ( !m_com )
  {
    return empty;
  }

  //- check hardware access perf!
  yat::Timer t;
  
  try
  {
    //- command need a response?
    if ( cmd.rfind("?") != std::string::npos )
    {
      response = m_com->write_read(cmd);
      //- erase caractere
      response.erase(response.find("\n"));
      // INFO_STREAM << "HPManager::write_read() cmd [" << cmd << "] response [" << response << "]." << std::endl;
    }
    else
    {
      //- only write command
      m_com->write(cmd);
      // INFO_STREAM << "HPManager::write_read() cmd [" << cmd << "] (no response)." << std::endl;
    }
    INFO_STREAM << "HPManager::write_read() cmd [" << cmd 
                << "] response [" << response << "]." 
                << " done in " << t.elapsed_msec() << " milliseconds"
                << std::endl;
    //- HP34401 needs 20milliseconds between two commands
    yat::ThreadingUtilities::sleep(SLEEP_IN_SECONDS, SLEEP_BETWEEN_TWO_ACTIONS);

  }
  catch (Tango::DevFailed&  df)
  {
    ERROR_STREAM << "HPManager::write_read() -> caugth DevFailed for command [" << cmd << "]." << std::endl;
    ERROR_STREAM << df << std::endl;
    return empty;
  } 
  catch (...)
  {
    ERROR_STREAM << "HPManager::write_read() -> caugth [...] for command [" << cmd << "]." << std::endl;
    return empty;
  }

  return response;
}

//-----------------------------------------------
//- exec_low_level_command
//- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
//-----------------------------------------------
// std::string HPManager::exec_low_level_command (std::string cmd)
// {
//   DEBUG_STREAM << "HPManager::exec_low_level_command for command [" << cmd << "\n]" << std::endl;
//   std::string response;
//   cmd += "\n";
//   //- this is a reading, response expected
//   if (cmd.find ("?") != std::string::npos)
//   {
//     this->write_read (cmd, response);
//     return response;
//   }
//   //- else it is a command, HW does not send a response, return just a " "
//   else
//   {
//     this->write (cmd);
//     return std::string(" ");
//   }
// }

} //- namespace
