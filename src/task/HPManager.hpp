
//- Project : HP34401 Multimeter
//- file : HPManager.hpp
//- threaded reading of the HW


#ifndef __HP_MANAGER_H__
#define __HP_MANAGER_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include "Communication.hpp"


namespace HP34401A_ns
{

//------------------------------------------------------------------------
//- HPManager Class
//- read the HW 
//------------------------------------------------------------------------
class HPManager : public yat4tango::DeviceTask
{
public :

  //- Constructeur/destructeur
  HPManager ( Tango::DeviceImpl * host_device,
              std::string proxy_name,
              std::vector<std::string> config);

  virtual ~HPManager ();

  //- the number of triggers (default = 1)
  void set_trigger_count (Tango::DevUShort trg_cnt=1);
  Tango::DevUShort get_trigger_count ();

  //- the number of samples per trigger (default = 1)
  void set_sample_count (Tango::DevUShort samp_cnt=1);
  Tango::DevUShort get_sample_count ();

  //- the integration time in Number of Power Line Cycle
  void set_nplc (double);
  double get_nplc ();

  //- the trigger source (default = IMMediate)
  void set_trigger_source (std::string trg_src="IMM");
  std::string get_trigger_source ();

  //- get the actual resolution
  double get_resolution ();

  //- get output range
  double get_range ();

  //- autorange cmds
  void enable_autorange  ();
  void disable_autorange ();
  bool is_autorange_enabled();

  //- returns the HP state and status
  Tango::DevState get_state_status (std::string& status);

  double get_data () {
    yat::AutoMutex<> guard(m_data_mutex);
    return m_output_value;
  }

  //- configure HP34401
  void configure ();

protected:
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg);

  //- mutex
  yat::Mutex m_data_mutex;
  
private :
  //- 
  std::string write_read (std::string);

  //- start integration
  void start_integration();

  //- not implemented yet!
  //- ... need GpibDevice to be changed
  //- stop integration
  // void stop_integration();

  //- read back output value
  void read_value ();

  //- system error
  void get_error ();

  //- the host device 
  Tango::DeviceImpl * host_dev;

  //- communication device proxy name
  std::string m_proxy_name;

  //- config : cmds to send to the HP34401
  std::vector<std::string> m_vec_config;

  //- the communication link
  yat::SharedPtr<Communication> m_com;

  //- trigger count
  Tango::DevUShort m_trigger_count;

  //- samples count (nb samples per trigger)
  Tango::DevUShort m_sample_count;

  //- integration time (Number of Power Line Cycles)
  double m_nplc;

  //- trigger source
  std::string m_trigger_src;

  //- resolution (nb digit)
  double m_resolution;

  //- range
  double m_range;

  //- autorange?
  bool m_autorange_enabled;

  //- data
  double m_output_value;

  //- error(s)
  std::string m_error;
};

}//- namespace

#endif //- __HP_MANAGER_H__
