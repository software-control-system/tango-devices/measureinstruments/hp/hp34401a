/*----- PROTECTED REGION ID(HP34401A.h) ENABLED START -----*/
//=============================================================================
//
// file :        HP34401A.h
//
// description : Include file for the HP34401A class
//
// project :     multimeter
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef HP34401A_H
#define HP34401A_H

#include <tango.h>
#include <DeviceProxyHelper.h>
#include "task/HPManager.hpp"


/*----- PROTECTED REGION END -----*/	//	HP34401A.h

/**
 *  HP34401A class description:
 *    this device controls the HP34401A multimeter. It is designed to read the output value
 *    in relation to the chosen mode.
 */

namespace HP34401A_ns
{
/*----- PROTECTED REGION ID(HP34401A::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	HP34401A::Additional Class Declarations

class HP34401A : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(HP34401A::Data Members) ENABLED START -----*/

//	Add your own data members
public:



/*----- PROTECTED REGION END -----*/	//	HP34401A::Data Members

//	Device property data members
public:
	//	CommunicationProxyName:	name of the device which handles the communication protocol
	string	communicationProxyName;
	//	Configuration:	Set of SCPI commands to configure the multimeter
	vector<string>	configuration;

//	Attribute data members
public:
	Tango::DevDouble	*attr_outputValue_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	HP34401A(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	HP34401A(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	HP34401A(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~HP34401A() {delete_device();}


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : HP34401A::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute outputValue related methods
 *	Description: according to the HP34401A multimeter function used,\nthis attribute gets the measure.
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_outputValue(Tango::Attribute &attr);
	virtual bool is_outputValue_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : HP34401A::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 *	@returns State Code
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command AutoRangeOFF related method
	 *	Description: Disables Autorange
	 *
	 */
	virtual void auto_range_off();
	virtual bool is_AutoRangeOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoRangeON related method
	 *	Description: Enables Autorange
	 *
	 */
	virtual void auto_range_on();
	virtual bool is_AutoRangeON_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoRangeEnabled related method
	 *	Description: Returns the auto range state (ON or OFF)
	 *
	 *	@returns AutoRange state (ON or OFF)
	 */
	virtual Tango::DevString auto_range_enabled();
	virtual bool is_AutoRangeEnabled_allowed(const CORBA::Any &any);
	/**
	 *	Command GetTiggerSource related method
	 *	Description: Returns the trigger source as string
	 *
	 *	@returns Returns the configured trigger source
	 */
	virtual Tango::DevString get_tigger_source();
	virtual bool is_GetTiggerSource_allowed(const CORBA::Any &any);
	/**
	 *	Command SetTriggerSource related method
	 *	Description: Sets the trigger source
	 *
	 *	@param argin The desired trigger source (between : BUS, IMMediate or EXTernal)
	 */
	virtual void set_trigger_source(Tango::DevString argin);
	virtual bool is_SetTriggerSource_allowed(const CORBA::Any &any);
	/**
	 *	Command GetTriggerCount related method
	 *	Description: Get the number of triggers the multimeter will accept before returning to the
	 *               “idle” state.
	 *
	 *	@returns The configured trigger count
	 */
	virtual Tango::DevUShort get_trigger_count();
	virtual bool is_GetTriggerCount_allowed(const CORBA::Any &any);
	/**
	 *	Command SetTriggerCount related method
	 *	Description: Set the number of triggers the multimeter will accept before returning to the
	 *               “idle” state. Allowed values in the range [1, 50000].
	 *
	 *	@param argin Allowed values in the range [1, 50000].
	 */
	virtual void set_trigger_count(Tango::DevUShort argin);
	virtual bool is_SetTriggerCount_allowed(const CORBA::Any &any);
	/**
	 *	Command GetSampleCount related method
	 *	Description: Returns the number of readings (samples) the multimeter takes per trigger.
	 *
	 *	@returns The number of readings (samples) the multimeter takes per trigger.
	 */
	virtual Tango::DevUShort get_sample_count();
	virtual bool is_GetSampleCount_allowed(const CORBA::Any &any);
	/**
	 *	Command SetSampleCount related method
	 *	Description: Set the number of readings (samples) the multimeter takes per trigger.
	 *
	 *	@param argin The number of readings (samples) the multimeter takes per trigger.
	 */
	virtual void set_sample_count(Tango::DevUShort argin);
	virtual bool is_SetSampleCount_allowed(const CORBA::Any &any);
	/**
	 *	Command GetResolution related method
	 *	Description: Returns the actual digits resolution.
	 *
	 *	@returns The selected resolution (depends also on the NPLC!)
	 */
	virtual Tango::DevDouble get_resolution();
	virtual bool is_GetResolution_allowed(const CORBA::Any &any);
	/**
	 *	Command GetRange related method
	 *	Description: Returns the actual range
	 *
	 *	@returns The selected range
	 */
	virtual Tango::DevDouble get_range();
	virtual bool is_GetRange_allowed(const CORBA::Any &any);
	/**
	 *	Command GetNPLC related method
	 *	Description: Returns the integration time per point (in NPLC)
	 *
	 *	@returns NPLC value
	 */
	virtual Tango::DevDouble get_nplc();
	virtual bool is_GetNPLC_allowed(const CORBA::Any &any);
	/**
	 *	Command SetNPLC related method
	 *	Description: Sets the integraion time per point (in NPLC)
	 *
	 *	@param argin Allowed values {0.02|0.2|1|10|100}
	 */
	virtual void set_nplc(Tango::DevDouble argin);
	virtual bool is_SetNPLC_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : HP34401A::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(HP34401A::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
protected :	
  HPManager * m_task;

  std::string m_status;

/*----- PROTECTED REGION END -----*/	//	HP34401A::Additional Method prototypes
};

/*----- PROTECTED REGION ID(HP34401A::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	HP34401A::Additional Classes Definitions

}	//	End of namespace

#endif   //	HP34401A_H
